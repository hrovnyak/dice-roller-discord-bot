use std::{future::Future, time::Duration};

use log::{error, info, trace};
use once_cell::sync::Lazy;
use pest::{iterators::Pairs, pratt_parser::PrattParser, Parser};
use pest_derive::Parser;
use rand::{rngs::StdRng, Rng, SeedableRng};
use serenity::{
    async_trait,
    http::Http,
    model::prelude::Message,
    prelude::{Context, EventHandler, GatewayIntents, Mutex},
    Client,
};

// https://discord.com/api/oauth2/authorize?client_id=1062115263690055730&permissions=0&scope=bot

const MAX_ROLLS: u32 = 4;

macro_rules! bug {
    (unexpected_input, $input: expr, $expected: expr$(,)?) => {
        Error::FoundABug(format!(
            "The parser didn't expect the input `{}` to be possible in this position, expected {}",$input,$expected
        ), line!())
    };

    (parse_number, $number: expr, $expected: expr$(,)?) => {
        return Error::FoundABug(format!(
            "The parser should've rejected {} as a number, expected {}", $number, $expected
        ), line!())
    };

    (expected_child, $expected: expr, $under: expr$(,)?) => {
        return Error::FoundABug(format!("The parser didn't output {} under {}", $expected, $under), line!())
    }
}

#[derive(Parser)]
#[grammar = "grammar.pest"]
struct DiceCommandParser;

#[derive(Debug)]
enum DiceExpr {
    Number(f64),
    Roll(i32),
    BinOp {
        lhs: Box<DiceExpr>,
        op: Op,
        rhs: Box<DiceExpr>,
    },
}

impl DiceExpr {
    fn eval(&self, rng: &mut impl Rng) -> (f64, String) {
        match self {
            DiceExpr::Number(num) => (*num, num.to_string()),
            DiceExpr::Roll(num) => {
                let roll = rng.gen_range(1..=*num);

                (roll as f64, format!("{roll}"))
            }
            DiceExpr::BinOp { lhs, op, rhs } => {
                let (lhs_val, mut lhs_str) = lhs.eval(rng);
                let (rhs_val, mut rhs_str) = rhs.eval(rng);

                if lhs.needs_parens(*op) {
                    lhs_str = format!("({lhs_str})");
                }

                if rhs.needs_parens(*op) {
                    rhs_str = format!("({rhs_str})");
                }

                let ret = match op {
                    Op::Add => lhs_val + rhs_val,
                    Op::Sub => lhs_val - rhs_val,
                    Op::Mul => lhs_val * rhs_val,
                    Op::Div => lhs_val / rhs_val,
                };

                (
                    ret,
                    format!(
                        "{lhs_str} {} {rhs_str}",
                        match op {
                            Op::Add => "+",
                            Op::Sub => "-",
                            Op::Mul => "*",
                            Op::Div => "/",
                        }
                    ),
                )
            }
        }
    }

    fn needs_parens(&self, super_op: Op) -> bool {
        match (self, super_op) {
            (DiceExpr::BinOp { lhs: _, op, rhs: _ }, super_op) => op.needs_parens(super_op),
            _ => false,
        }
    }
}

#[derive(Clone, Copy, Debug)]
enum Op {
    Add,
    Sub,
    Mul,
    Div,
}

impl Op {
    fn needs_parens(self, super_op: Op) -> bool {
        use Op::*;

        matches!(
            (self, super_op),
            (Add, Mul) | (Sub, Mul) | (Add, Div) | (Sub, Div)
        )
    }
}

static PARSER: Lazy<PrattParser<Rule>> = Lazy::new(|| {
    use pest::pratt_parser::{Assoc::*, Op};
    use Rule::*;

    PrattParser::new()
        .op(Op::infix(add, Left) | Op::infix(sub, Left))
        .op(Op::infix(mul, Left) | Op::infix(div, Left))
});

fn parse_expr(pairs: Pairs<Rule>) -> Result<DiceExpr, Error> {
    PARSER
        .map_primary(|primary| {
            Ok(match primary.as_rule() {
                Rule::number => DiceExpr::Number(
                    primary
                        .as_str()
                        .parse()
                        .map_err(|_| bug!(parse_number, primary, "a literal number"))?,
                ),
                Rule::roll => {
                    let number_text = primary
                        .into_inner()
                        .next()
                        .ok_or_else(|| bug!(expected_child, "a number", "a roll"))?;

                    DiceExpr::Roll(
                        number_text
                            .as_str()
                            .parse()
                            .map_err(|_| bug!(parse_number, number_text, "a dice type"))?,
                    )
                }
                _ => parse_expr(primary.into_inner())?,
            })
        })
        .map_infix(|lhs, op, rhs| {
            Ok(DiceExpr::BinOp {
                lhs: Box::new(lhs?),
                op: match op.as_rule() {
                    Rule::add => Op::Add,
                    Rule::sub => Op::Sub,
                    Rule::mul => Op::Mul,
                    Rule::div => Op::Div,
                    _ => return Err(bug!(unexpected_input, op, "an operation")),
                },
                rhs: Box::new(rhs?),
            })
        })
        .parse(pairs)
}

fn parse_command(command: &str) -> Result<(DiceExpr, u32), Error> {
    let mut pairs =
        DiceCommandParser::parse(Rule::command, command).map_err(|_| Error::NotARollMessage)?;

    let mut command = pairs
        .next()
        .ok_or_else(|| {
            Error::FoundABug(
                "The parser didn't output any information".to_owned(),
                line!(),
            )
        })?
        .into_inner();

    let expr = command
        .next()
        .ok_or_else(|| {
            Error::FoundABug(
                "The message shouldn't have been parsed without an expression given".to_owned(),
                line!(),
            )
        })?
        .into_inner();

    let maybe_amt_pair = command.next();

    let amt = match maybe_amt_pair {
        None => 1,
        Some(amt_pair) => match amt_pair.as_rule() {
            Rule::once => 1,
            Rule::twice => 2,
            Rule::thrice => 3,
            Rule::number_amt => {
                let amt_str = amt_pair
                    .into_inner()
                    .next()
                    .ok_or_else(|| bug!(expected_child, "a number", "a number amount"))?;

                amt_str
                    .as_str()
                    .parse::<u32>()
                    .map_err(|_| bug!(parse_number, amt_str, "an amount of times to roll"))?
            }
            Rule::EOI => 1,
            _ => {
                return Err(bug!(
                    unexpected_input,
                    amt_pair,
                    "an amount of times to roll",
                ))
            }
        },
    };

    if amt > MAX_ROLLS {
        return Err(Error::TooManyRolls(amt));
    }

    Ok((parse_expr(expr)?, amt))
}

enum Error {
    NotARollMessage,
    TooManyRolls(u32),
    MessageTooLong,
    FoundABug(String, u32),
}

async fn send_err(http: &Http, msg: &Message, fut: impl Future<Output = Result<(), Error>>) {
    if let Err(e) = fut.await {
        let err_msg = match e {
            Error::NotARollMessage => return,
            Error::TooManyRolls(amt) => {
                format!("Please roll {MAX_ROLLS} or fewer, {amt} is too many")
            }
            Error::MessageTooLong => {
                "The resulting message is too long for discord to handle".to_owned()
            }
            Error::FoundABug(bug, line) => {
                error!(
                    "Someone found a bug in the code. Input: {}, Error: {bug} on line {line}",
                    msg.content
                );

                format!("You found a bug in the code: {bug}")
            }
        };

        if let Err(e) = msg.reply(http, err_msg).await {
            error!("Sending an error message failed: {e}");
        }
    }
}

struct Bot {
    rng: Mutex<StdRng>,
}

impl Bot {
    fn new() -> Bot {
        Bot {
            rng: Mutex::new(StdRng::from_entropy()),
        }
    }
}

#[async_trait]
impl EventHandler for Bot {
    async fn message(&self, ctx: Context, msg: Message) {
        send_err(&ctx.http, &msg, async {
            let (expr, times) = parse_command(&msg.content)?;

            let mut rng = self.rng.lock().await;

            let rolls = (0..times).map(|_| expr.eval(&mut *rng)).collect::<Vec<_>>();

            let max_len = rolls.iter().fold(0, |a, v| a.max(v.1.len()));

            let reply = rolls
                .into_iter()
                .map(|(val, expr)| {
                    format!(
                        "`{expr}{}`    **{val}**",
                        (0..max_len - expr.len()).map(|_| ' ').collect::<String>()
                    )
                })
                .collect::<Vec<_>>()
                .join("\n");

            msg.reply(&ctx.http, reply)
                .await
                .map_err(|_| Error::MessageTooLong)?;

            trace!("Someone rolled a dice: {}", msg.content);

            Ok(())
        })
        .await;
    }
}

fn main() {
    pretty_env_logger::init();

    info!("Starting the bot");

    let runtime = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .expect("Creating the runtime shouldn't have failed");

    runtime.block_on(async {
        info!("Waiting for internet");
        while reqwest::get("https://discord.com").await.is_err() {
            tokio::time::sleep(Duration::from_secs(1)).await;
        }
        info!("Internet is available, starting");

        let intents = GatewayIntents::MESSAGE_CONTENT
            | GatewayIntents::GUILD_MESSAGES
            | GatewayIntents::DIRECT_MESSAGES;

        let mut client = Client::builder(include_str!("../token.txt"), intents)
            .event_handler(Bot::new())
            .await
            .expect("Creating client shouldn't have failed");

        if let Err(e) = client.start().await {
            error!("There was an error in the client: {:?}", e);
        }
    });
}
